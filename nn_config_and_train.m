%% INITIALIZE AND TRAIN NEURAL NETWORK %%
%For Windows, change all '/' to '\'

%To use this program:
%Click Run.
%Select which output and inputs to use, and whether to use LMs with leaf
%changes or not.
%Three plots and the training and neural network objects will be generated 
%and saved.

%To modify network parameters, see lines 23 to  43 in the nn_create function.
%Alpha, n, trainFcn, divideParam, and the number of validation checks can be
%changed, and r-squared can be used instead of performance to choose the best
%network.

clear

%% Add folders to path
fileID = mfilename('fullpath'); %main folder file path
A_split = split(fileID,'/');
A = join(A_split(1:end-1),'/');
A = A{1,1};
addpath(genpath(strcat(A,'/functions'))); %functions folder
% addpath(genpath(strcat(A,'/split_data'))); %input and target data folder

%% Select data
[in_name,out_name,yr,time,out_type,modifier,col] = data_select();
% cd(strcat(A,'/split_data/',modifier)); %folder containing modified data, if modifier is given
filename = uigetfile({strcat(A,'/*.*')},'Select data file'); %Note: must have columns in proper order
import = importdata(filename);
data = import.data;

%% Split data into two sets (stage 0 and 1)
%time=0; %manually for now

%% CODE TEST
%Uses default inputs and output is sum of inputs
% out_name = 'tester';

%% Train networks and select best
% input = xlsread(strcat('inputs',in_name,yr))';
% targets = xlsread(strcat('outputs',out_name,yr))';
col = col(col~=0); %Remove zeros
input = zeros(length(data),length(col));
for i = 1:length(col)
    if col(i) > 0
        input(:,i) = data(:,col(i));
    end
end
targets = data(:,13);
%Remove NANs
nanrows = any(isnan(input), 2);
input(nanrows, :)  = [];
targets(nanrows,:) = [];
input = input';
targets = targets';

%% Split data
%If the data are split into two sets, one for stage 0 and one for stage 1
if time==0 %stage 0 and 1
    ind0 = input(1,:)==0;
    ind1 = input(1,:)==1;
    input0 = input(2:end,ind0); %Remove stage manually for now
    input1 = input(2:end,ind1); %"
    targets0 = targets(:,ind0);
    targets1 = targets(:,ind1);
    
    [perf_best0,Rsq_best0,net_best0,tr_best0,n_best0] = nn_create(input0,targets0);
    [perf_best1,Rsq_best1,net_best1,tr_best1,n_best1] = nn_create(input1,targets1);
else
    [perf_best,Rsq_best,net_best,tr_best,n_best] = nn_create(input,targets);
end

%% Save plots, nets and training data
if strcmp(out_name, 'tester')
    modifier = 'test';
end
if time==0 %stage 0 and 1
    dir = strcat(A,'/stage0'); %change base directory
    saver(dir,input0,targets0,tr_best0,net_best0,n_best0,out_type,in_name,out_name,yr,modifier);
    close all;
    
    dir = strcat(A,'/stage1'); %change base directory
    saver(dir,input1,targets1,tr_best1,net_best1,n_best1,out_type,in_name,out_name,yr,modifier);
    close all;
else
    saver(A,input,targets,tr_best,net_best,n_best,out_type,in_name,out_name,yr,modifier);
%     close all; %Comment out if you want to look at plots rather than just save them
end

%% Fiteval
%Run fiteval for results (only for test data)
cd(strcat(A,'/FITEVAL3_osx'));
if time==0 %stage 0 and 1
    run_fiteval(input0,targets0,net_best0,tr_best0);
    close all;
    
    cd(strcat(A,'/FITEVAL3_osx'));
    run_fiteval(input1,targets1,net_best1,tr_best1);
    close all;   
else
    run_fiteval(input,targets,net_best,tr_best);
    close all;
end

%% Return to initial folder
cd(A);
%Ready to run program again

disp('Done!')
