%% RUN PREVIOUSLY SAVED NEURAL NETWORK FOR B/LL SWP ANALYSIS %%
% MAC VERSION %
%For Windows, change all '/' to '\'

%To use this program:
%Click Run.

clear

%% Add folders to path
fileID = mfilename('fullpath'); %main folder file path
A_split = split(fileID,'/');
A = join(A_split(1:end-1),'/');
A = A{1,1};
addpath(genpath(strcat(A,'/functions'))); %functions folder
addpath(genpath(strcat(A,'/nets'))); %nets folder
addpath(genpath(strcat(A,'/split_data'))); %input and output data folder

%% Select data
modifier = 'SP_rosetta';
cd(strcat(A,'/split_data/',modifier)); %folder containing modified data, if modifier is given
%Best performing combination
in_name = 'VPDnochg';
out_name = 'TdTlnochg';
yr = '2017';
input = xlsread(strcat('inputs',in_name,yr))';
targets = xlsread(strcat('outputs',out_name,yr))';
inputBSWP = xlsread(strcat('inputs',in_name,yr,'_BSWP'))';
inputLLSWP = xlsread(strcat('inputs',in_name,yr,'_LLSWP'))';

n_best = 3; %manually input for now
star = '**'; %add * if asterisk was added to mark best performing net
filename = strcat(num2str(n_best),'hid_',in_name,out_name,yr,modifier,star,'.mat');

%% Select net
cd(strcat(A,'/nets')); %nets folder
%filename = uigetfile({strcat(A,'/nets/*.*')},'Select network');
net = importdata(filename);

%% Select training data
cd(strcat(A,'/tr')); %tr folder
%filename = uigetfile({strcat(A,'/tr/*.*')},'Select network');
tr = importdata(filename);

%% Outputs
out = net(input); %Generate outputs
outBSWP = net(inputBSWP);
outLLSWP = net(inputLLSWP);

% tsOut = out(tr.testInd); %Output for test data only
% tsTarg = targets(tr.testInd); %Targets for test data only

%% Get corresponding JD data
cd(strcat(A,'/split_data/',modifier)); %folder containing modified data, if modifier is given
in_nameJD = 'VPDJDnochg';
inputJD = xlsread(strcat('inputs',in_nameJD,yr))';
JD = inputJD(1,:);

%% Average for each JD
inds = find(JD==229); %Fix this later
averages = [];
for i = 1:inds(1)
    date = JD(i);
    date_ind = JD == date;
    avg_SWP = mean(input(1,date_ind));
    avg_BSWP = mean(inputBSWP(1,date_ind));
    avg_LLSWP = mean(inputLLSWP(1,date_ind));
    avg_TdTl = mean(out(1,date_ind));
    avg_Base_TdTl = mean(outBSWP(1,date_ind));
    avg_LL_TdTl = mean(outLLSWP(1,date_ind));
    averages = [averages; date avg_SWP avg_BSWP avg_LLSWP avg_TdTl avg_Base_TdTl avg_LL_TdTl]; 
end

%% Plots
% plot(JD,out,'o',JD,targets,'+'); %Plot all output and target data over time
% legend('Outputs','Targets');
% title('All Data'); xlabel('Julian Date'); ylabel('Tdiffdry (�C)');
% tsJD = JD(tr.testInd); %JD for test data only
% figure(4)
% plot(tsJD,tsOut,'o',tsJD,tsTarg,'+'); %Plot test output and target data over time
% legend('Outputs','Targets');
% title('Test Data Only'); xlabel('Julian Date'); ylabel('Tdiffdry (�C)');

% figure(1)
% plot(1:length(inputBSWP),inputBSWP(1,:),1:length(inputLLSWP),inputLLSWP(1,:),1:length(outBSWP),outBSWP,1:length(outLLSWP),outLLSWP);
% legend('BWSP','LLSWP','Baseline Td - Tl', 'LL Td - Tl');

%% Return to initial folder
cd(A);
%Ready to run program again

