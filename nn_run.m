%% RUN PREVIOUSLY SAVED NEURAL NETWORK %%
% MAC VERSION %
%For Windows, change all '/' to '\'

%To use this program:
%Click Run.
%Select which output and inputs to use, and whether to use LMs with leaf
%changes or not.
%Three plots and the trained neural network object will be generated and saved.

clear

%% Add folders to path
fileID = mfilename('fullpath'); %main folder file path
A_split = split(fileID,'/');
A = join(A_split(1:end-1),'/');
A = A{1,1};
addpath(genpath(strcat(A,'/functions'))); %functions folder
addpath(genpath(strcat(A,'/nets'))); %nets folder
% addpath(genpath(strcat(A,'/split_data'))); %input and output data folder

%% Select data
[in_name,out_name,yr,time,out_type,modifier,col] = data_select();
% cd(strcat(A,'/split_data/',modifier)); %folder containing modified data, if modifier is given
filename = uigetfile({strcat(A,'/*.*')},'Select data file'); %Note: must have columns in proper order
import = importdata(filename);
data = import.data;

%Shortcut for best performing combination
% in_name = 'VPDstagenochg';
% out_name = 'TdTlnochg';
% yr = '2017';

% input = xlsread(strcat('inputs',in_name,yr))';
% targets = xlsread(strcat('outputs',out_name,yr))';
col = col(col~=0); %Remove zeros
input = zeros(length(data),length(col));
for i = 1:length(col)
    if col(i) > 0
        input(:,i) = data(:,col(i));
    end
end
targets = data(:,13);
%Remove NANs
nanrows = any(isnan(input), 2);
input(nanrows, :)  = [];
targets(nanrows,:) = [];
input = input';
targets = targets';

n_best = 3; %manually input for now
star = '**'; %add * if asterisk was added to mark best performing net
filename = strcat(num2str(n_best),'hid_',in_name,out_name,yr,modifier,star,'.mat');

%% Select net
cd(strcat(A,'/nets')); %nets folder
%filename = uigetfile({strcat(A,'/nets/*.*')},'Select network');
net = importdata(filename);

%% Select training data
cd(strcat(A,'/tr')); %tr folder
%filename = uigetfile({strcat(A,'/tr/*.*')},'Select network');
tr = importdata(filename);

%% Retrain?
% perf_best = 1; %Initialize best values
% Rsq_best = 0;
% n_best = 0;
%     for i = 1:5
%         [net_tr,tr] = train(net,input,targets);
%         perf = tr.best_vperf; %Select by best val performance
%         out = net_tr(input);
%         ind = tr.testInd; %Test data index
% %         ind = tr.valInd; %Validation data index
% %         Rsq = rsquared(targets',out');
%         Rsq = rsquared(targets(ind)',out(ind)'); %test data r squared value
% %         if Rsq > Rsq_best %Uncomment to use r-squared to choose best network instead
%         if perf < perf_best %Comment out to use r-squared
%             perf_best = perf;
%             Rsq_best = Rsq;
%             net_best = net_tr;
%             tr_best = tr;
%             n_best = n;
% %             alpha_best = alpha;
%         end
%     end

%% Plots
%Add this to main program eventually
[fig1,fig2] = nn_plot(input,targets,tr,net,n_best,out_type); %Plot the usual NN plots
out = net(input); %Generate output
tsOut = out(tr.testInd); %Output for test data only
tsTarg = targets(tr.testInd); %Targets for test data only
% plot(1:length(targets),targets,1:length(out),out); %plot output and target data series
%Get corresponding JD data
cd(strcat(A,'/split_data/',modifier)); %folder containing modified data, if modifier is given
in_nameJD = 'VPDJDnochg'; %Input manually for now
inputJD = xlsread(strcat('inputs',in_nameJD,yr))';
JD = inputJD(1,:);
figure(3)
plot(JD,out,'o',JD,targets,'+'); %Plot all output and target data over time
legend('Outputs','Targets');
title('All Data'); xlabel('Julian Date'); ylabel('Tdiffdry (�C)');
tsJD = JD(tr.testInd); %JD for test data only
figure(4)
plot(tsJD,tsOut,'o',tsJD,tsTarg,'+'); %Plot test output and target data over time
legend('Outputs','Targets');
title('Test Data Only'); xlabel('Julian Date'); ylabel('Tdiffdry (�C)');
    
%% Fiteval
%Run fiteval for results
cd(strcat(A,'/FITEVAL3_osx'));
run_fiteval(input,targets,net,tr); %Only for test data
close all;

%% Return to initial folder
cd(A);
%Ready to run program again

disp('Done!')
