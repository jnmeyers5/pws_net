%% RUN FITEVAL FOR PREVIOUSLY SAVED NEURAL NETWORK, SPLIT BY STAGE %%
% MAC VERSION %
%For Windows, change all '/' to '\'

%To use this program:
%Click Run.
%Select which output and inputs to use, and whether to use LMs with leaf
%changes or not.
%Three plots and the trained neural network object will be generated and saved.

clear

%% Add folders to path
fileID = mfilename('fullpath'); %main folder file path
A_split = split(fileID,'/');
A = join(A_split(1:end-1),'/');
A = A{1,1};
addpath(genpath(strcat(A,'/functions'))); %functions folder
addpath(genpath(strcat(A,'/nets'))); %nets folder
addpath(genpath(strcat(A,'/split_data'))); %input and output data folder

%% Select data
[in_name,out_name,yr,time,out_type,modifier] = data_select();
cd(strcat(A,'/split_data/',modifier)); %folder containing modified data, if modifier is given
%Shortcut for best performing combination
% in_name = 'VPDstagenochg';
% out_name = 'TdTlnochg';
% yr = '2017';
input = xlsread(strcat('inputs',in_name,yr))';
targets = xlsread(strcat('outputs',out_name,yr))';

cd(strcat(A,'/stage0/nets'));
filename0net = uigetfile({strcat(A,'/stage0/nets/*.*')},'Select stage 0 network');
net0 = importdata(filename0net);
cd(strcat(A,'/stage0/tr'));
filename0tr = uigetfile({strcat(A,'/stage0/tr/*.*')},'Select stage 0 training data');
tr0 = importdata(filename0tr);
cd(strcat(A,'/stage1/nets'));
filename1net = uigetfile({strcat(A,'/stage1/nets/*.*')},'Select stage 1 network');
net1 = importdata(filename1net);
cd(strcat(A,'/stage1/tr'));
filename1tr = uigetfile({strcat(A,'/stage1/tr/*.*')},'Select stage 1 training data');
tr1 = importdata(filename1tr);

%% Divide data
time = 0;
%If the data is split into two sets, one for stage 0 and one for stage 1
if time==0 %stage 0 and 1
    ind0 = input(1,:)==0;
    ind1 = input(1,:)==1;
    input0 = input(2:end,ind0); %Remove stage manually for now
    input1 = input(2:end,ind1); %"
    targets0 = targets(:,ind0);
    targets1 = targets(:,ind1);
end

%% Fiteval
%Run fiteval for results
cd(strcat(A,'/FITEVAL3_osx'));
run_fiteval(input0,targets0,net0,tr0); %Only for test data
run_fiteval(input1,targets1,net1,tr1); %Only for test data
close all;

