%% Plot a trendline and R^2 value on plot

function trendline(xData, yData)
%Add trendline
%Least squares
coef=[xData ones(length(xData),1)]\yData;
xfit=[min(xData) max(xData)];
yfit=coef(1)*xfit + coef(2);
%Quadratic (polyfit)  (better R^2, but some lines concave down)
% p = polyfit(xData,yData,2);
% xfit = linspace(min(xData), max(xData), length(xData));
% yfit = polyval(p,xfit);
plot(xfit,yfit,'r-') 

%Find R^2
yfit2=coef(1)*xData + coef(2);
% yfit2 = polyval(p,xData);
Rsq = 1 - sum((yData - yfit2).^2)/sum((yData - mean(yData)).^2);

dim = [0.6 0.6 .3 .3];
str = ['R^2= ', num2str(Rsq), char(10), 'y= ', num2str(coef(1)), '*x + ', num2str(coef(2))];
annotation('textbox',dim,'String',str,'FitBoxToText','on');

end