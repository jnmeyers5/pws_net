function [ Julian_Day ] = Date_to_Julian(month,day,leap)
%This is a simple function for converting a calender day to a Julian day
% The inputs are (month,day,leap). Input the month for month, day for day,
% and for leap put 'y' if a leap year, or 'n' if not.
md=[31,59,90,120,151,181,212,243,273,304,334,365];  %Days of the year if it is not a leap year
mdl=[31,60,91,121,152,182,213,244,274,305,335,366]; %Days of the year if it is a leap year

if (leap=='y'|| leap=='Y')
    Julian_Day=mdl(month-1)+day;
else
    Julian_Day = md(month-1)+day;
end

end

