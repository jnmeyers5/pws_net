%% Plots
%Generate plots

function [fig1,fig2] = nn_plot(input,targets,tr_best,net_best,n_best,out_type)

out = net_best(input);
trOut = out(tr_best.trainInd);
vOut = out(tr_best.valInd);
tsOut = out(tr_best.testInd);
trTarg = targets(tr_best.trainInd);
vTarg = targets(tr_best.valInd);
tsTarg = targets(tr_best.testInd);

%Regression
figure(1)
% fig = plotregression(targets,out);
fig1 = regplot(trTarg, trOut, 'Train', vTarg, vOut, 'Validation', tsTarg, tsOut, 'Testing');

%Performance
figure(2)
fig2 = plotperform(tr_best);

%Compare
% figure(2);
% plot(targets); hold on; plot(out)
% ylabel('Td-Tl'); xlabel('Time'); title(['Td-Tl Network, Levenberg-Marquardt, ' num2str(n_best) ' hidden neurons'])
% legend('Targets', 'Outputs')

%Regression with R Squared
% figure(3)
% fig3 = plot(targets,out,'o'); hold on;
% trendline(targets',out');
% title([out_type ' Network, ' net_best.trainFcn ', ' num2str(n_best) ' hidden neurons']);
% xlabel('Targets'); ylabel('Outputs')

end