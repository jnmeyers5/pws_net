classdef LM < handle
    % Leaf monitor class
    % ==================
    properties
    % Raw data
        Ta
        Tl
        hum
        PAR
        wind
        time
        JD
        sDay
        name
    % Computed
        VPD
        Y
        Tl_adj
        offs
        hum_avg
        PAR_avg
        VPD_avg
        Y_avg
        Tdiff_avg
%         Tl_flt   %TL filtered
%         Ta_flt   %TA filtered
%         TAc      %TA calibrated
%         TLc      %TL calibrated 
%         CWSI
%         MCWSI
    % Calibration related
%         TL2   
%         TA2   
%         time_TLc
%         time_TAc
%         time_spl_TLc
%         time_spl_TAc
%         DPAR_TLc
%         DPAR_TAc
%         Ta_spl
%         Tadry_spl
%         Tl_spl
%         Tlsat_spl
%         reg_TL
%         reg_TA
%         rsq_TLc
%         rsq_TAc
%         parselect
    end
    
    properties(Dependent)
        Tdiff
        Tdiff_flt
        Tdiffraw
    end
    
    methods
        function thisLM = LM(Ta,Tl,hum,PAR,wind,time,JD,sDay,name)
            if nargin == 9
                thisLM.Ta = Ta;
                thisLM.Tl = Tl;
                thisLM.hum = hum;
                thisLM.PAR = PAR;
                thisLM.wind = wind;
                thisLM.time = time;
                thisLM.JD = JD;
                thisLM.sDay = sDay;
                thisLM.name = name;
            end
        end
        
        function Tdiff = get.Tdiff(thisLM)
            Tdiff = thisLM.Ta - thisLM.Tl_adj(:,2);
        end
        
        function Tdiffraw = get.Tdiffraw(thisLM)
            Tdiffraw = thisLM.Ta - thisLM.Tl;
        end
        
%         function Tdiff_flt = get.Tdiff_flt(thisLM)
%             Tdiff_flt = thisLM.Ta_flt - thisLM.Tl_flt;
%         end

        
    end
end

