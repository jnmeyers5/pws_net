%% Average Moisture Each Day
%Input is array of time vs soil moisture content for one node, on one day
%Columns 3 and 4 give hour and minute

function array_out = dailyavg(array_in)
    f = @noon2four; %Call function to filter for only times from 12 to 4 pm
    midday = f(array_in);
    if isempty(midday) == 1
        midday = array_in;
    end
    average=mean(midday(1:end,2)); %Find the avg
    date=midday(1,1);
    array_out = [date,average];
    
end
