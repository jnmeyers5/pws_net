classdef SM < handle
    % Leaf monitor class for soil moisture
    % ==================
    properties
    % Raw data
        watercont
        time
        JD
        JDfull
        sDay
        node
        name
    % Computed
%         daily
        dailyavg
%         deltasm
%         cumsm
%         cumeto
%         cumsm_all
%         cumeto_all
    end
    
    methods
        function thisSM = SM(watercont,time,JD,JDfull,sDay,node,name)
            if nargin == 7
                thisSM.watercont = watercont;
                thisSM.time = time;
                thisSM.JD = JD;
                thisSM.JDfull = JDfull;
                thisSM.sDay = sDay;
                thisSM.node = node;
                thisSM.name = name;
            end
        end
   
    end
end