%% Create and Save Plots, Nets and Training Data
function saver(dir,input,targets,tr,net,n_best,out_type,in_name,out_name,yr,modifier)
    %create and save plots
    cd(strcat(dir,'/plots'));
    [fig1,fig2] = nn_plot(input,targets,tr,net,n_best,out_type);
    saveas(fig1,strcat(in_name,out_name,yr,modifier,'_regression.jpg'))
    saveas(fig2,strcat(in_name,out_name,yr,modifier,'_perform.jpg'))
    
    %save net
    cd(strcat(dir,'/nets'));
    save(strcat(num2str(n_best),'hid_',in_name,out_name,yr,modifier),'net')
    
    %save training data
    cd(strcat(dir,'/tr'));
    save(strcat(num2str(n_best),'hid_',in_name,out_name,yr,modifier),'tr')
end
