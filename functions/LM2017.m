classdef LM2017 < handle
    % Leaf monitor class
    % ==================
    properties
    % Raw data
        Ta
        Tl
        Tdry    %JM 1/12/17
        hum
        PAR
        wind
        time
        JD
        sDay
        name
    % Computed
        VPD
        Y
        Tl_adj
        Tdry_adj
        offs
        offsdry
        ratio
        hum_avg
        PAR_avg
        VPD_avg
        Y_avg
        Tdiff_avg
        Td_tl_avg
        ratio_avg
%         Tl_flt   %TL filtered
%         Ta_flt   %TA filtered
%         Tdry_flt  %Tdry filtered    JM 1/30/17
%         TAc      %TA calibrated
%         TLc      %TL calibrated 
%         Tdryc     %Tdry calibrated   JM 1/30/17
%         CWSI
%         dryCWSI   %JM 1/23/17
%         MCWSI
%         dryMCWSI   %JM 1/23/17
    % Calibration related
%         TL2   
%         TA2   
%         time_TLc
%         time_TAc
%         time_spl_TLc
%         time_spl_TAc
%         DPAR_TLc
%         DPAR_TAc
%         Ta_spl
%         Tadry_spl
%         Tl_spl
%         Tlsat_spl
%         reg_TL
%         reg_TA
%         rsq_TLc
%         rsq_TAc
%         parselect
    end
    
    properties(Dependent)
        Tdiff
%         Tdiff_flt
        Tdiffraw
        Td_tl   %JM
        Td_tlraw
%         Tdiffdry_flt  %JM
    end
    
    methods
        function thisLM = LM2017(Ta,Tl,Tdry,hum,PAR,wind,time,JD,sDay,name) %JM
            if nargin == 10     %JM
                thisLM.Ta = Ta;
                thisLM.Tl = Tl;
                thisLM.Tdry = Tdry;
                thisLM.hum = hum;
                thisLM.PAR = PAR;
                thisLM.wind = wind;
                thisLM.time = time;
                thisLM.JD = JD;
                thisLM.sDay = sDay;
                thisLM.name = name;
            end
        end
        
        function Tdiff = get.Tdiff(thisLM)
            Tdiff = thisLM.Ta - thisLM.Tl_adj(:,2);
        end
        
        function Tdiffraw = get.Tdiffraw(thisLM)
            Tdiffraw = thisLM.Ta - thisLM.Tl;
        end
        
%         function Tdiff_flt = get.Tdiff_flt(thisLM)
%             Tdiff_flt = thisLM.Ta_flt - thisLM.Tl_flt;
%         end
        
        function Td_tl = get.Td_tl(thisLM)
            Td_tl = thisLM.Tdry_adj(:,2) - thisLM.Tl_adj(:,2);     %JM
        end
        
        function Td_tlraw = get.Td_tlraw(thisLM)
            Td_tlraw = thisLM.Tdry - thisLM.Tl;
        end
        
%         function Tdiffdry_flt = get.Tdiffdry_flt(thisLM)
%             Tdiffdry_flt = thisLM.Ta_flt - thisLM.Tdry_flt;     %JM
%         end

        
    end
end

