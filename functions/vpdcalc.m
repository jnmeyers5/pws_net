%% Calculate VPD

function VPD = vpdcalc(Tair, RH)
    VPD = 0.6108*exp((17.27*Tair)./(Tair+237.3)).*(1-RH/100);
end
