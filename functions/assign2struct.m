%% Sort data into a structure
%Each field is one node

function data=assign2struct(import, yr, dates, info_table, type)

if type == 'LM' | type == 'SM'
    %From static model
    serial=x2mdate(import.data(:,1),0); % converts Excel time, num(:,1), into serial number dates 
    dates_str=datestr(serial); % converts serial number dates into real time string
    dates_vec=datevec(dates_str); % splits real time string into 6 column matrix (year, month, day, hour, minute, second)

    ind1 = dates_vec(:,1) == yr;
    temp_vec = dates_vec(ind1,:);
    datenow = temp_vec(1,:);

    JD=floor(serial-(datenum(datenow(1),1,1)-1));

    datasel=[JD import.data(:,2:end)];
    alldata = [dates_vec datasel];

% elseif type == 'SM'
%     %Create Time Matrix
%     %Want time in numerical form
%     temp_vec = datevec(import.textdata(2:end,1),'yyyy-mm-dd');
%     ind1 = temp_vec(:,1) == yr;
%     temp_vec = temp_vec(ind1,:);
%     datenow = temp_vec(1,:);
% %     datenow=datevec(import.textdata(2,1),'yyyy-mm-dd');
%     JD=datenum(import.textdata(2:end,1),'yyyy-mm-dd')-(datenum(datenow(1),1,1)-1);
%     
%     textsel=import.textdata(2:end,1);
%     datasel=[JD import.data];
%     alldata = [datevec(textsel(:,1)) datasel];
end

%Convert date array to JD
JD_range = dates;
%Remove dates not in specified date array
ind2 = zeros(length(JD),1);
for i = 1:length(JD_range)
    ind2(JD==JD_range(i))=1;
end
ind2 = logical(ind2);
ind_both = ind1 & ind2;
alldata = alldata(ind_both,:);

%Separate data and sort into categories in the class structure
if type == 'LM'
    act=cell2mat(info_table(:,4));
    info_table(logical(~act),:) = [];     %Delete LM data which are inactived
    for i=1:size(info_table,1)  
        namecell = info_table(i,1);
        LMs = namecell{1};     % When a cell is called using {} produce a string 
        LMnum= str2double(namecell{1}(3:end));  % or a double if is a number
        
        %LM40 IR sensors switched July 24 2017, JD 205
        if yr == 2017 && LMnum == 40
            ind40 = alldata(:,7) > 205;
            tl_old = alldata(:,9);
            tdry_old = alldata(:,13);
            tl_new = tdry_old(ind40);
            tdry_new = tl_old(ind40);
            tl_old(ind40) = tl_new;
            tdry_old(ind40) = tdry_new;
            alldata(:,9) = tl_old;
            alldata(:,13) = tdry_old;
        end
        
        dataLM = alldata;
        id=zeros(size(dataLM(:,18)));       %id is in row 18
        id(dataLM(:,18)==LMnum)=1;
        dataLM(logical(~id),:) = [];

        hr=dataLM(:,4)+dataLM(:,5)/60+dataLM(:,6)/3600; %hours
%         hr = dataLM(:,4);
        dst=datestr([dataLM(:,1),dataLM(:,2),dataLM(:,3),dataLM(:,4),dataLM(:,5),dataLM(:,6)]); %date(string)  
        if yr == 2017
            data.(LMs) = LM2017(dataLM(:,11),dataLM(:,9),dataLM(:,13),dataLM(:,10),dataLM(:,15),dataLM(:,12),hr,dataLM(:,7)+hr/24,dst,LMs);
                            %Ta,Tl,Tdry,hum,PAR,wind,time,JD,sDay,name
        else
            data.(LMs) = LM(dataLM(:,11),dataLM(:,9),dataLM(:,10),dataLM(:,15),dataLM(:,12),hr,dataLM(:,7)+hr/24,dst,LMs); 
                            %Ta,Tl,hum,PAR,wind,time,JD,sDay,name
        end
            
        if isempty(dataLM)
            fprintf('_red','%s is empty\n',LMs);%q=1;
        else
            fprintf('%s added\n',LMs)
        end
    end
elseif type == 'SM'
    act=cell2mat(info_table(:,5));
    info_table(logical(~act),:) = [];     %Delete SM data which are inactived
    for i=1:size(info_table,1)  
        SMcell = info_table(i,1);
        namecell = info_table(i,2);
        SMs = SMcell{1};     % Call cell with {}
        nodes = namecell{1};
        dataLM = alldata;
    %     node_num= str2double(nodecell{1});
        id=zeros(size(dataLM(:,8)));       %node id is in row 8
        if i ~= 1 && strcmp(nodes, info_table{i-1,2})
            SMprev = info_table{i-1,1};
            id(dataLM(:,8)==SMs | dataLM(:,8)==SMprev) = 1;
            SMs = [SMs, SMprev];
        else
            id(dataLM(:,8)==SMs)=1;
        end
        dataLM(logical(~id),:) = [];
        
        hr=dataLM(:,4)+dataLM(:,5)/60+dataLM(:,6)/3600; %hours
        dst=datestr([dataLM(:,1),dataLM(:,2),dataLM(:,3),dataLM(:,4),dataLM(:,5),dataLM(:,6)]); %date(string)    
        data.(nodes) = SM(dataLM(:,9),hr,dataLM(:,7),dataLM(:,7)+hr/24,dst,SMs,nodes);  
                %watercont,time,JD,JDfull,sDay,node,name
                
        if isempty(dataLM)
            fprintf('%s is empty\n',nodes);%q=1;
        else
            fprintf('%s added\n',nodes)
        end
        
    end
end
                
end