%% Removes all rows except those with hours between noon and 4 pm
%Takes in array with three columns, with time in the third column

function array_out = noon2four(array_in)
    logic = array_in(:,3) >= 12 & array_in(:,3) < 16;
    col2 = array_in(1:end,2); %Separate second and third columns for sorting purposes
    col3 = array_in(1:end,3);
    array_out = [array_in(logic), col2(logic), col3(logic)];
end
