function [ E,VPD ] = StaticModel_calcs(Tl,Ta,RH)
%This function calculates es(Tl)-e(Ta) and the vapor pressure deficit. 
%   You must have the Tleaf, Tair, and Relative Humidity data. You must
%   have the same number of data points for each of these types of data,
%   otherwise matrix dimensions will not agree.

xl=(17.27.*Tl)./(Tl+237.3); %xl, the exponent part of es(Tl)

xa=(17.27.*Ta)./(Ta+237.3); %xa, the exponent part of es(Ta)

es_Tl=0.6108.*exp(xl); %es(Tl). Partial vapor pressure of the water within the stomatal cavity

es_Ta=0.6108.*exp(xa); %es(Ta). Partial vapor pressure of the water in the ambient air

e_Ta=es_Ta.*(RH./100); %e(Ta)

E=es_Tl-e_Ta; %es(Tl)-e(Ta)

VPD=es_Ta.*(1-RH./100); %VPD = vapor pressure deficit

end

