%% Run Fiteval
%Set up data and run the fiteval program

function run_fiteval(input,targets,net_best,tr_best)

out = net_best(input);
tsOut = out(tr_best.testInd);
tsTarg = targets(tr_best.testInd);

% in = [targets',out']; %All outputs and targets
in = [tsTarg',tsOut']; %Test outputs and targets only
dlmwrite('fiteval.in',in); %Write results to file with default file name

fiteval(); %Call fiteval function, which will run the application

end