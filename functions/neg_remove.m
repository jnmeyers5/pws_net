%% Negative Remover
%Takes an array with two or more columns
%Removes rows if the second column has a negative value in that row

function array_out = neg_remove(array_in)
    logic = array_in >=0; %Create logic function for when array_in is positive or 0
    cols2 = array_in(1:end,2:end); %Separate second and later columns for sorting purposes
    col1 = array_in(logic(1:end,2));
    cols2 = cols2(logic(1:end,2),:);
    array_out = [col1,cols2];
end