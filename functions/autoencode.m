%% Autoencoder

% Encode extra Tdiff data too!!
%First create training and test data - half of the original data each
%(length = 217)

x = training;

hiddenSize = 10;
autoenc = trainAutoencoder(x',hiddenSize,...
        'EncoderTransferFunction','satlin',...
        'DecoderTransferFunction','purelin');%,...
%         'L2WeightRegularization',0.01,...
%         'SparsityRegularization',4,...
%         'SparsityProportion',0.10);

xtest = test;
    
xReconstructed = predict(autoenc,xtest');

mseError = mse(x-xReconstructed');

figure;
plot(xtest,'r.');
hold on
plot(xReconstructed','go');
    