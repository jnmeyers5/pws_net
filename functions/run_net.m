
input = inputsstagenochg2017';
targets = outputsTdTlnochg2017';

% [net,tr] = train(net_best,input,targets);

out = net_best(input);

figure(1)
plotregression(targets,out)

figure(2); n_best = 3;
plot(targets,out,'o'); hold on;
trendline(out',targets');
title(['Td-Tl Network, ' 'trainlm' ', ' num2str(n_best) ' hidden neurons']);
xlabel('Targets'); ylabel('Outputs')

figure(3)
plotperform(tr);

out = out';

