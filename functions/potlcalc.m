%% Calculate potential of air

% Y = R*Ta*ln(RH/100)/Vcc
% 
% Where R is universal gas constant (82.05 atm-cc/mole/K)
% Ta is ambient temperature in deg K
% Vcc = molar volume of water vapor, 18 cc/mole
% RH = % relative humidity.
% 
% Y is in atmosphere. Multiply by 0.98692327 to get bar.
% Convert to MPa by dividing the results by 10.   

function Y = potlcalc(Ta,RH)

R = 82.05; %atm-cc/mole/K
Vcc = 18; %cc/mole

Ta_K = Ta + 273.15; %Convert Ta to Kelvin

Y_atm = R*Ta_K.*log(RH/100)/Vcc;

Y = Y_atm*0.98692327/10; %Convert to MPa

end
