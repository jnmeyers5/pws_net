%% Remove outliers, depending on output data

function [new_in, new_out] = outlier_rem(inputs, outputs)
    z = zscore(outputs);
    idx = z < -2;
    new_in = inputs;
    new_out = outputs;
    new_in(idx,:) = [];
    new_out(idx) = [];
end
