%% Configure Neural Networks

function [perf_best,Rsq_best,net_best,tr_best,n_best] = nn_create(input,targets)

perf_best = 1; %Initialize best values
Rsq_best = 0;
n_best = 0;

alpha = 10; %1 for data with no noise to 10 for typically noisy data

in_sz = size(input); %Parameters for calculating hidden neuron number
tgt_sz = size(targets);

% N_i=(Number of Training Data Pairs)/((Number of Input Neurons +Number of Output Neurons)� alpha )
n = round(0.7*in_sz(2)/((in_sz(1)+tgt_sz(1))*alpha));  % number of neurons

    net = feedforwardnet(n);  % create a neural network
    
%     net.layers{1}.transferFcn = 'poslin'; %ReLu layer

    net = configure(net, input, targets);  % configure the neural network for this dataset

    net = init(net);

    net.trainFcn = 'trainlm';  %'trainlm', 'trainbr' or 'trainbfg' (Levenberg-Marquardt, Bayesian Regularization or quasi-Newton)
    %Default data division: 70% training, 15% validation, 15% test
%    net.divideParam.trainRatio = 0.6; % 60% training data - Uncomment to change input data division
%    net.divideParam.valRatio = 0.3; % 30% validation data
%    net.divideParam.testRatio = 0.1; % 10% test data
%    net.trainParam.max_fail = 10; %Uncomment to change number of validation checks

  %Cycle through 5 trainings to determine best of these
    for i = 1:5
        [net_tr,tr] = train(net,input,targets);
        perf = tr.best_vperf; %Select by best val performance
        out = net_tr(input);
        ind = tr.testInd; %Test data index
%         ind = tr.valInd; %Validation data index
%         Rsq = rsquared(targets',out');
        Rsq = rsquared(targets(ind)',out(ind)'); %test data r squared value
        if Rsq > Rsq_best %Uncomment to use r-squared to choose best network instead
%         if perf < perf_best %Comment out to use r-squared
            perf_best = perf;
            Rsq_best = Rsq;
            net_best = net_tr;
            tr_best = tr;
            n_best = n;
%             alpha_best = alpha;
        end
    end

end
