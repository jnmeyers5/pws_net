%% Select what data to use

function [in_name,out_name,yr,time,out_type,modifier,col] = data_select()

% [out_type,ok] = listdlg('PromptString', 'Select Output Type:',...
%     'ListString', {'Ta-Tl','Td-Tl'},...
%     'InitialValue', 2,...
%     'SelectionMode', 'single');
out_type=2; %Only option is Td-Tl for now
[in_type,ok] = listdlg('PromptString', 'Select Micromet. Variable:',...
    'ListString', {'RH','VPD','air potential (Y)','none'},...
    'InitialValue', 2,...
    'SelectionMode', 'single');
% [soil_water,ok] = listdlg('PromptString', 'Select Soil Water Variable:',...
%     'ListString', {'Soil Water Content','Soil Water Potential'},...
%     'InitialValue', 2,...
%     'SelectionMode', 'single');
[time,ok] = listdlg('PromptString', 'Time Column:',...
    'ListString', {'none','JD','stage'},...
    'InitialValue', 1,...
    'SelectionMode', 'single');
[yr_choose,ok] = listdlg('PromptString', 'Select Year: ',...
    'ListString', {'2016','2017'},...
    'InitialValue', 2,...
    'SelectionMode', 'single');
[mod_choose,ok] = listdlg('PromptString', 'Select Modifier: ',...
    'ListString', {'None', 'No SM', 'No PAR', 'No SM or PAR', 'No SWP', 'Soil potential'},...
    'InitialValue', 6,...
    'SelectionMode', 'single'); %Removed 'SM w/ same points as SP' as an option

%% Load appropriate data based on selections
col=zeros(1,5); %create vector to choose columns for input
%Categories: Time Column, SWP, Soil, PAR, Micrometeorological
col(2) = 6; %SWP automatically selected, and is in 6th column of full datasheet
col(3) = 7; %Soil water content automatically selected
col(4) = 9; %PAR automatically selected

if yr_choose == 1
    yr = '2016';
elseif yr_choose == 2
    yr = '2017';
end

%Leaf changes removed or not will depend on the input datasheet
% inputs for the neural net
if time == 1 %none
    if in_type == 1 %RH
        in_name = 'RH';
        col(5) = 10; %Column for RH
    elseif in_type == 2 %VPD
        in_name = 'VPD';
        col(5) = 11; %Column for VPD
    elseif in_type == 3 %air potential (Y)
        in_name = 'Y';
        col(5) = 12; %Column for Y
    elseif in_type == 4 %none
        in_name = '';
    end
elseif time == 2 %JD
    col(1) = 1; %Column for JD
    if in_type == 1 %RH
        in_name = 'RHJD';
        col(5) = 10; %Column for RH
    elseif in_type == 2 %VPD
        in_name = 'VPDJD';
        col(5) = 11; %Column for VPD
    elseif in_type == 3 %air potential (Y)
        in_name = 'YJD';
        col(5) = 12; %Column for Y
    elseif in_type == 4 %none
        in_name = 'JD';
    end
elseif time == 3 %stage
    col(1) = 2; %Column for stage
    if in_type == 1 %RH
        in_name = 'RHstage';
        col(5) = 10; %Column for RH
    elseif in_type == 2 %VPD
        in_name = 'VPDstage';
        col(5) = 11; %Column for VPD
    elseif in_type == 3 %air potential (Y)
        in_name = 'Ystage';
        col(5) = 12; %Column for Y
    elseif in_type == 4 %none
        in_name = 'stage';
    end
end
    % targets for the neural net
    if out_type == 1 %Ta-Tl
        out_name = 'TaTl';
    elseif out_type == 2 %Td-Tl
        out_name = 'TdTl';
    end
% modifier
if mod_choose == 1 %none
    modifier = '';
elseif mod_choose == 2
    modifier = 'No_SM';
    col(3) = 0;
elseif mod_choose == 3
    modifier = 'No_PAR';
    col(4) = 0;
elseif mod_choose == 4
    modifier = 'No_SM_PAR';
    col(3) = 0;
    col(4) = 0;
elseif mod_choose == 5
    modifier = 'No_SWP';
    col(2) = 0;
elseif mod_choose == 6 %Soil potential
    modifier = 'SP_rosetta';
    col(3) = 8; %Column for soil potential
% elseif mod_choose == 7 %Soil moisture with the same days removed as soil potential
%     modifier = 'SM_samepts_SP';
      %Only relevant if you are estimating soil water potl from water
      %content
end
