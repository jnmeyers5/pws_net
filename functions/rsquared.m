%% Calculate R Squared

function Rsq = rsquared(xData,yData)
%Least squares
coef=[xData ones(length(xData),1)]\yData;

%Find R^2
yfit=coef(1)*xData + coef(2);
Rsq = 1 - sum((yData - yfit).^2)/sum((yData - mean(yData)).^2);

end
