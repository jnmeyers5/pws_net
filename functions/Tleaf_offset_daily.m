function [ IRT_adj, offset ] = Tleaf_offset_daily( hour, Tair, IRT, JDD, JD, leafmon, p )
% This function adjusts for the offset in Tair-Tleaf. A unique offset is 
% calculated each day. We know that Tair
% should not be lower than Tleaf at night. We are trusting the number on
% the Tair sensor and blaming the Tleaf sensor for the negative offset
% in Tair-Tleaf during the night.
% Input: IRT data (must run this function for every IRT sensor you have on
% the leaf monitor. The 2017 version of the leaf monitor has two IRT
% sensors, so this function needs to be run for both IRT sensors. 

%Calculate the Tdiff offset by summing all of the Tdiff between the time
%period chosen below. 

JD_unique = unique(JD);

i=zeros(size(JD_unique,1),1);

sum=zeros(size(JD_unique,1),1);

offset=zeros(size(JD_unique,1),1);

IRT_adj=zeros(size(IRT,1),1); % pre-allocate array of zeros the size of the number of IRT data points

for k = 1:size(JD_unique,1) % for each unique day
    
    n = find(JD == JD_unique(k)); % find the indices of all the data points that correspond to day k
    
    for j=n(1):n(end) % for each index corresponding to day k

         if hour(j) >= 1 && hour(j) < 4 % if the hour is between 1 and 4 AM

             i(k) = i(k) + 1; % add one to the counter
 
             sum(k) = sum(k) + (Tair(j) - IRT(j)); % add the offset between Tair and the IRT to the sum

         end

    end

    offset(k) = sum(k)/i(k); % calculate the offset by dividing the sum of the offsets by the number of data  points between 1 and 4 AM

    for j=n(1):n(end) % for each index corresponding to day k

        IRT_adj (j) = IRT(j) + offset(k); % + or - the offset to every data point of the IRT sensor. (Note: If the offset is negative, the addition will turn into a subtraction sign).

    end


end

IRT_adj = [JD,IRT_adj];
offset = [JD_unique,offset];

if p=='y'
    
    figure(leafmon) % figure # is the leaf monitor #
    plot(JDD,Tair-IRT,... 
        JDD,Tair-IRT_adj(:,2)) % plot original IRT data versus offset adjusted IRT data
    xlabel('Julian Decimal Date')
    ylabel('IRT Sensor Reading')
    title(['LM ' num2str(leafmon) ': Correcting the Offset in IRT Sensor 1-4 AM'])
    legend('Original Data','Offset Adjusted')
    xlim([140,143])
    
    hold on
    
    n=find(hour >= 1 & hour < 4); % find all data points between 1 and 4 AM
    scatter(JDD(n),Tair(n)-IRT(n),'xb') % plot in 1-4 AM data points with a different symbol to show what data points were used for adjusting the offset
    scatter(JDD(n),Tair(n)-IRT_adj(n,2),'xr')
    
    grid on
    
end

end

