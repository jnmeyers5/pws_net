%% Split individual node data into daily data
%Takes array with date in the first column and water content in the second

function array_out = datesplit(array_in)
    G = findgroups(array_in(1:end,1)); %Input each of these single date arrays into dailyavg
    f = @dailyavg;
    array_out = splitapply(f,array_in,G);
end
