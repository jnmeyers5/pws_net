%% Compute zone averages for parameter
%

function [z1_avg,z2_avg,g1_avg,g2_avg] = zoneavg(names,data,table,dates,param)
    sz = length(dates);
    z1_avg = zeros(sz,2);
    z2_avg = zeros(sz,2);
    g1_avg = zeros(sz,2);
    g2_avg = zeros(sz,2);

    if strcmp(param, 'dailyavg') %Soil moisture
        j = 3;
    else
        j = 2;
    end      

    for i = 1:length(dates)
        c1 = 0;
        c2 = 0;
        cg1 = 0;
        cg2 = 0;
        for k = 1:length(names)
            if table{k,j+2} == 0 %Skip inactive LMs
                continue
            elseif k ~= 1 && strcmp(names{k},names{k-1}) %Don't count duplicates
                continue
            end
            data_ind = data.(names{k}).(param);
            ind = dates(i) == data_ind(:,1);
            if all(~ind) %Array is all zeros, this node/LM does not have date
                continue
            end
            if table{k,j} == 'L' && table{k,j+1} == '1' %Zone 1
                z1_avg(i,:) = z1_avg(i,:) + data_ind(ind,:);
                c1 = c1 + 1; %Count number of LMs
            elseif table{k,j} == 'L' && table{k,j+1} == '2' %Zone 2
                z2_avg(i,:) = z2_avg(i,:) + data_ind(ind,:);
                c2 = c2 + 1;
            elseif table{k,j} == 'G' && table{k,j+1} == '1' %Grower 1
                g1_avg(i,:) = g1_avg(i,:) + data_ind(ind,:);
                cg1 = cg1 + 1;
            elseif table{k,j} == 'G' && table{k,j+1} == '2' %Grower 2
                g2_avg(i,:) = g2_avg(i,:) + data_ind(ind,:);
                cg2 = cg2 + 1;
            end
        end
        z1_avg(i,:) = z1_avg(i,:)/c1;
        z2_avg(i,:) = z2_avg(i,:)/c2;
        g1_avg(i,:) = g1_avg(i,:)/cg1;
        g2_avg(i,:) = g2_avg(i,:)/cg2;
    end       
    
    %Old z2_avg = z2_avg + data.(names{k}).(param);
%                 c2 = c2 + 1;
end