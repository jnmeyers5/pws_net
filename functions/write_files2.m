%% Write to Excel files

% function write_files(LMdata, names, tableData1)

for i = 1:length(names)
    if tableData1{i,4} == 0
        continue
    else
%         xlswrite('Tl Offset Adjusted', LMdata.(names{i}).Tl_adj, names{i});
%         xlswrite('Tdry Offset Adjusted', LMdata.(names{i}).Tdry_adj, names{i});
%         xlswrite('Offsets', LMdata.(names{i}).offs, names{i});
%         xlswrite('Offsets, Dry', LMdata.(names{i}).offsdry, names{i});
        xlswrite('Potential of Air', LMdata.(names{i}).Y, names{i});
    end
end

% end
