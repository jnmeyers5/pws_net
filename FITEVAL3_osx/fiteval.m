function fiteval(varargin)
%
%   FITEVAL: Program for evaluating the match between observed 
%   and calculated values
% 
% - A file is required containing the observed and calculated 
%   values to be evaluated. This file must contain two columns.
%   The first column are the observed values and the second 
%   column the calculated values.
% 
% - The input file may contain missing values, that must be 
%   denoted as nan
% 
% - The filename can be passed as an argument (default filename
%   is "fiteval.in"): fiteval filename
% 
% - The program finishes after closing the figure.
% 
% - After running FITEVAL, it performs the goodness-of-fit e
%   evaluation providing a portable data file (pdf) containing: 
%    a) a plot of observed vs. computed values illustrating the
%       match on the 1:1 line; 
%    b) the calculation of Ceff and RMSE and their corresponding 
%       confidence intervals of 95%; 
%    c) the qualitative goodness-of-fit interpretation based on 
%       the established classes; 
%    d) a verification of the presence of bias or the possible 
%       presence of outliers; 
%    e) the plot of the Ceff cumulative probability function 
%       superimposed on the Ceff class regions; 
%    f) a plot illustrating the evolution of the observed and
%       computed values. 
% 
% - Additionally, the numerical output is stored in an ascii text
%   file.
% 
% - The above-mentioned plots can be obtained also as separated 
%   files in the specified (as argument) graphic format ('eps', 
%   'pdf', 'jpg', 'tiff, or 'png')
% 
% - Removing repeated cases in the observed and calculated values
%   is possible by passing as second argument NOREP. For example: 
%   fiteval data_ex3.in NOREP
%   The program indicates the number of removals only if repeated
%   cases are present.
%
% - The fitevalconfig.txt file is required when you want to run 
%   FITEVAL with other threshold values or for calculating Legates 
%   and McCabe (1999) modified form of the coefficient of efficiency
%   (E1) instead of NSE. This file contains six lines specifying: 
%   Acceptable NSEthreshold, Good NSEthreshold, Very good NSEthreshold,
%   relative bias threshold value (%), the option for computing E1,
%   bootstrap type, figures� font size, and the option for canceling
%   the on-screen display of the graphical output. The corresponding 
%   default values are 0.65, 0.80, 0.90, 5, 0, 0, 10 and 0, respectively.
%
% - FITEVAL can apply Efron and Tibshirani (1993) bootstrap or Politis
%   and Romano (1994) block bootstrap when dealing with time series.
%   The latter is the default option.
%
% - See fitevaloptions_help.pdf for accounting for uncertainty in fiteval