%% Data Processing for Input to Neural Network
% MAC VERSION %
%For Windows, change all '/' to '\'

%Generates data needed for input to neural networks

clear

%% Add folders to path
fileID = mfilename('fullpath'); %main folder file path
A_split = split(fileID,'/');
A = join(A_split(1:end-1),'/');
A = A{1,1};
addpath(genpath(strcat(A,'/functions')));
addpath(genpath(strcat(pwd,'/files')));

%% Load data
yr = '2017'; %This must be changed before running this for another year!!
load(['dates_', yr]);
load(['tableData1_', yr]);
load(['infoTable_', yr]);

disp('Select leaf monitor data');
filename = uigetfile({strcat(A,'/files/*.*')},'Select leaf monitor data');
import = importdata(filename);
disp('Select soil water data');
filename = uigetfile({strcat(A,'/files/*.*')},'Select soil water data');
import2 = importdata(filename);

%% Assign data to structures
LMdata = assign2struct(import,str2double(yr),dates,tableData1,'LM'); %Assign data to structure
names = tableData1(:,1);
SMdata = assign2struct(import2,str2double(yr),dates,infoTable,'SM'); %Assign data to structure
names2 = infoTable(:,2);

%% Compute daily averages for LM Data
for i = 1:length(names)
    if tableData1{i,4} == 0
        continue
    else
        JDD = LMdata.(names{i}).JD;
        JD = floor(JDD);
        time = floor(LMdata.(names{i}).time); %Hour
        Ta = LMdata.(names{i}).Ta;
        leafmon = str2double(regexprep(names{i},'LM',''));
        
        [LMdata.(names{i}).Tl_adj,LMdata.(names{i}).offs] = Tleaf_offset_daily(time,Ta,LMdata.(names{i}).Tl,JDD,JD,leafmon,'n'); %Adjust Tdiff for offset
        if strcmp(yr,'2017')  %Adjust dry data for offset if 2017
            [LMdata.(names{i}).Tdry_adj,LMdata.(names{i}).offsdry] = Tleaf_offset_daily(time,Ta,LMdata.(names{i}).Tdry,JDD,JD,leafmon,'n');
        end
        
        RH = LMdata.(names{i}).hum;
        Tl = LMdata.(names{i}).Tl_adj(:,2);
        
        LMdata.(names{i}).hum_avg = datesplit([JD,RH,time]);
        LMdata.(names{i}).PAR_avg = datesplit([JD,LMdata.(names{i}).PAR,time]);
        LMdata.(names{i}).Tdiff_avg = datesplit([JD,LMdata.(names{i}).Tdiff,time]);
        if strcmp(yr,'2017')
            LMdata.(names{i}).Td_tl_avg = datesplit([JD,LMdata.(names{i}).Td_tl,time]);
            LMdata.(names{i}).ratio = ratiocalc(LMdata.(names{i}).Td_tl,Tl,Ta,RH);
            LMdata.(names{i}).ratio_avg = datesplit([JD,LMdata.(names{i}).ratio,time]);
        end
        LMdata.(names{i}).Y = potlcalc(Ta,RH);
        LMdata.(names{i}).Y_avg = datesplit([JD,LMdata.(names{i}).Y,time]);
        LMdata.(names{i}).VPD = vpdcalc(Ta,RH);
        LMdata.(names{i}).VPD_avg = datesplit([JD,LMdata.(names{i}).VPD,time]);
    end
end

%% Compute daily average soil moisture content
for i = 1:length(names2)
    if infoTable{i,5} == 0
        continue
    else
        array_in = [SMdata.(names2{i}).JD,SMdata.(names2{i}).watercont,SMdata.(names2{i}).time];
        array_in = neg_remove(array_in); %Remove negative values
        SMdata.(names2{i}).dailyavg = datesplit(array_in); %Calculates daily average moisture cont
    end
end

%% Compute zone averages
avgData = struct('z1',[],'z2',[],'g1',[],'g2',[]);
[avgData.z1.hum,avgData.z2.hum,avgData.g1.hum,avgData.g2.hum] = zoneavg(names,LMdata,tableData1,dates,'hum_avg');
[avgData.z1.PAR,avgData.z2.PAR,avgData.g1.PAR,avgData.g2.PAR] = zoneavg(names,LMdata,tableData1,dates,'PAR_avg');
[avgData.z1.Tdiff,avgData.z2.Tdiff,avgData.g1.Tdiff,avgData.g2.Tdiff] = zoneavg(names,LMdata,tableData1,dates,'Tdiff_avg');
[avgData.z1.SM,avgData.z2.SM,avgData.g1.SM,avgData.g2.SM] = zoneavg(names2,SMdata,infoTable,dates,'dailyavg');

disp('Done!')
