%% Multiple Linear Regression %%
% MAC VERSION %
%For Windows, change all '/' to '\'

%To use this program:
%Click Run.
%Select which output and inputs to use, and whether to use LMs with leaf
%changes or not.
%(Does not currently work for ratio)

clear

%% Add folder to path
fileID = mfilename('fullpath'); %main folder file path
A_split = split(fileID,'/');
A = join(A_split(1:end-1),'/');
A = A{1,1};
addpath(genpath(strcat(A,'/functions')));
addpath(genpath(strcat(A,'/split_data')));

%% Select data
[in_name,out_name,yr,time,out_type] = data_select();

modifier = 'SP_rosetta'; %'', 'No_SM', 'No_PAR', 'No_SM_PAR', 'No_SWP', 'SP_rosetta'
cd(strcat(A,'/split_data/',modifier));

%% Fit datasets
input = xlsread(strcat('inputs',in_name,yr));
targets = xlsread(strcat('outputs',out_name,yr));

time = 0; %manually split data for now
%If the stage parameter is used, the data must be split into two sets, one
%for stage 0 and one for stage 1
if time==0 %split into two stages
    ind0 = input(:,1)==0;
    ind1 = input(:,1)==1;
    input0 = input(ind0,2:end); %manually remove stage for now
    input1 = input(ind1,2:end);
    targets0 = targets(ind0,:);
    targets1 = targets(ind1,:);
    %Comment out 2 of the following 3 models to choose which one to run:
%     mdl0 = fitrlinear(input0, targets0);
%     mdl1 = fitrlinear(input1, targets1);
%     L0 = loss(mdl0,input0,targets0);
%     L1 = loss(mdl1,input1,targets1);
% 
%     [b0,bint0,r0,rint0,stats0] = regress(targets0, input0);
%     [b1,bint1,r1,rint1,stats1] = regress(targets1, input1);
% 
    mdl0 = fitlm(input0, targets0);
    mdl1 = fitlm(input1, targets1);
    clc
    display(mdl0)
    display(mdl1)
    
else
    %Comment out 2 of the following 3 models to choose which one to run:
%     mdl = fitrlinear(input, targets);
%     L = loss(mdl,input,targets);
% 
%     [b,bint,r,rint,stats] = regress(targets, input); %Don't choose -
%     needs a column of 1's to be accurate, then should give same output as
%     fitlm

    mdl = fitlm(input, targets);
    display(mdl)
end

%Return to initial folder
cd(A);
%Ready to run program again
